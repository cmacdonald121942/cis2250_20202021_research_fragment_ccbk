package com.example.myfragmentapp1;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentCameron#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentCameron extends Fragment {

    //define variables
    private EditText enteredTime;
    private EditText enteredDate;
    private ToggleButton toggleButtonCM;
    private ImageButton imageButtonAndroidCM;
    private SeekBar seekBarCM;
    private ProgressBar progressBarCM;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentCameron() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentCameron.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCameron newInstance(String param1, String param2) {
        FragmentCameron fragment = new FragmentCameron();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentCameron newInstance() {
        FragmentCameron fragment = new FragmentCameron();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cameron, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("BJM","The first fragment is being created");
        //CM - Fill the variables.
        enteredTime = view.findViewById(R.id.editTextTimeCM);
        enteredDate = view.findViewById(R.id.editTextDateCM);
        imageButtonAndroidCM = view.findViewById(R.id.imageButtonAndroidCM);
        toggleButtonCM = view.findViewById(R.id.toggleButtonCM);
        seekBarCM = view.findViewById(R.id.seekBarCM);
        progressBarCM = view.findViewById(R.id.progressBarCM);

        progressBarCM.setMax(100);

        seekBarCM.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progressBarCM.setProgress(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //create listener for image button
        imageButtonAndroidCM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            String text ="Time:"+ enteredTime.getText().toString()
                       + "\n" + "Date: " + enteredDate.getText().toString();
                //display the entries then clear them
                Snackbar.make(view,text,Snackbar.LENGTH_LONG) .setAction("Action",null) .show();
                enteredTime.setText("");
                enteredDate.setText("");
            }
        });

        //solution referenced here https://stackoverflow.com/questions/11978880/how-to-change-color-of-the-toggle-button
        //create listener for toggle button
        toggleButtonCM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        //button is green when on
                        buttonView.setBackgroundColor(Color.GREEN);
                    } else {
                        //button is red when off
                        buttonView.setBackgroundColor(Color.RED);
                    }
                }
            });
    }
}