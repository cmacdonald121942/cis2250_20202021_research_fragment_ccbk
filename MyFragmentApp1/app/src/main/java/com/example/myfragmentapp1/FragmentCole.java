package com.example.myfragmentapp1;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.ToggleButton;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

/**
 * Author: Cole Gallant
 * For fragment activity
 * Finished on 20210131
 */
public class FragmentCole extends Fragment {

    //define variables
    private EditText phoneNumber;
    private EditText fullName;
    private Switch mute;
    private Button submit;
    private CheckBox favourite;
    private CheckBox family;
    private FloatingActionButton camButton;

    private String muted = "";
    private String isFavourite = "";
    private String isFamily = "";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentCole() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBlue.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentCole newInstance(String param1, String param2) {
        FragmentCole fragment = new FragmentCole();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentCole newInstance() {
        FragmentCole fragment = new FragmentCole();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cole, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("BJM","The first fragment is being created");
        //Filling variables by ID
        phoneNumber = view.findViewById(R.id.editTextPhoneCole);
        fullName = view.findViewById(R.id.editTextTextPersonName);
        mute = view.findViewById(R.id.switchCole);
        submit = view.findViewById(R.id.buttonSave);
        favourite = view.findViewById(R.id.checkBoxFav);
        family = view.findViewById(R.id.checkBoxFam);
        camButton = view.findViewById(R.id.floatingActionButtonPhoto);

        camButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Referenced from https://www.youtube.com/watch?v=-GNEmmjQGB0
                try {
                    Intent intent = new Intent();
                    intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivity(intent);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Logic for checkboxes
                    if (favourite.isChecked()) {
                        isFavourite = "Yes";
                    }
                    else {
                        isFavourite = "No";
                    }
                    if (family.isChecked()) {
                        isFamily = "Yes";
                    }
                    else {
                        isFamily = "No";
                    }

                    //Message displayed when add button is pressed
                    String text = "New Contact Added"
                            + "\nNumber:"+ phoneNumber.getText().toString()
                            + "\n" + "Name: " + fullName.getText().toString()
                            + "\n" + "Is a Favourite: " + isFavourite
                            + "\n" + "Is Family: " + isFamily;
                    //Display the contact with message
                    Snackbar.make(view,text,Snackbar.LENGTH_LONG) .setAction("Action",null) .show();
                }
            });

    }

}