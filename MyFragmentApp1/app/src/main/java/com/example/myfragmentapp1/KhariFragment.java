package com.example.myfragmentapp1;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link KhariFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KhariFragment extends Fragment {

    //KW select UI elements and store as variables
    private EditText userEmail;
    private EditText userPassword;

    private Button userButton;

    private  Button userClearButton;

    private ImageView image;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public KhariFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment KhariFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KhariFragment newInstance(String param1, String param2) {
        KhariFragment fragment = new KhariFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //KW select and store elements on create()
//        userEmail = view.findViewById(R.id.userEmail);
    }

        //KW - method to create a new instance of a fragment.
    public static KhariFragment newInstance() {
        KhariFragment fragment = new KhariFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_khari, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("BJM","The first fragment is being created");
            //KW - storing user variables.
            userEmail = view.findViewById(R.id.userEmail);
            userPassword = view.findViewById(R.id.userPassword);
            userButton = view.findViewById(R.id.submitBtn);
            userClearButton = view.findViewById(R.id.clearBtn);
            image = view.findViewById(R.id.imageView);

            userButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String text ="Username:"+ userEmail.getText().toString()
                            + "\n" + "Password: " + userPassword.getText().toString();

                    Log.d("KW", text);//KW logging information to log cat

                    //KW displaying username & password to snack bar
                    Snackbar.make(view,text,Snackbar.LENGTH_LONG)
                            .setAction("Action",null)
                            .show();
                }
            });



            userClearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userEmail.setText("");
                    userPassword.setText("");
                }
            });
    }
}