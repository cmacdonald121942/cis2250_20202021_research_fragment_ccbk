package com.example.myfragmentapp1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentBruce#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBruce extends Fragment {

    private Button btnSubmitBL;
    private CheckBox cbJapaneseStyleFriedPork;
    private CheckBox cbGrilledChickenDrumstick;
    private EditText editTextTextEmailAddressBL;
    private EditText editTextTextPostalAddressBL;
    private RatingBar ratingBarBL;
    private ProgressBar progressBarBL;

    private int progressStatusBL = 0;

    private Handler handlerBL = new Handler();


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentBruce() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBruce.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentBruce newInstance(String param1, String param2) {
        FragmentBruce fragment = new FragmentBruce();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentBruce newInstance() {
        FragmentBruce fragment = new FragmentBruce();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bruce, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSubmitBL = (Button) view.findViewById(R.id.btnSubmitBL);
        cbJapaneseStyleFriedPork = view.findViewById(R.id.cbJapaneseStyleFriedPork);
        cbGrilledChickenDrumstick = view.findViewById(R.id.cbGrilledChickenDrumstick);
        ;
        editTextTextEmailAddressBL = view.findViewById(R.id.editTextTextEmailAddressBL);
        ;
        editTextTextPostalAddressBL = view.findViewById(R.id.editTextTextPostalAddressBL);
        ;
        ratingBarBL = view.findViewById(R.id.ratingBarBL);
        ;
        progressBarBL = view.findViewById(R.id.progressBarBL);
        ;

        btnSubmitBL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (progressStatusBL < 100) {
                            progressStatusBL++;
                            android.os.SystemClock.sleep(30);
                            handlerBL.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBarBL.setProgress(progressStatusBL);
                                }
                            });
                        }
                        handlerBL.post(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                String msg = "";
                                if(cbJapaneseStyleFriedPork.isChecked()){
                                    msg += "Japanese Style Fried Pork Bento \n";
                                }
                                if(cbGrilledChickenDrumstick.isChecked()){
                                    msg += "Grilled Chicken Drumstick Bento \n";
                                }
                                msg += "Email: " + editTextTextEmailAddressBL.getText().toString() + "\n";

                                msg += "Address: " + editTextTextPostalAddressBL.getText().toString()+ "\n";

                                msg += "Rating: " + ratingBarBL.getRating();




                                builder.setTitle("Your Order has  been submitted");
                                builder.setMessage(msg);

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                                builder.show();
                            }
                        });
                    }
                }).start();
            }
        });
    }
}